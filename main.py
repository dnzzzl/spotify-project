from lib import SpotifyClient
sourceClient = ''
def main():
    global sourceClient
    input("Log into source account\n(PRESS ANY KEY)")
    sourceClient = LoginUser()
    input("Log into new account (All content will be overwritten)\n(PRESS ANY KEY)")
    secondClient = LoginUser(firstTime=False, firstClient=sourceClient)
    
    print("Starting Transfers:")
    print("Getting Liked Songs...")
    sourceTracks = sourceClient.GetSavedTracksList()
    print("Transfering Liked Songs...")
    secondClient.SaveTracks(sourceTracks)
    print("done.")
    print("Getting Liked Albums...")    
    sourceAlbums = sourceClient.GetAlbumsList()
    print("Saving Liked Albums...")
    secondClient.SaveAlbums(sourceAlbums)
    print("done.")
    print("Following Artists")
    followedArtists = sourceClient.GetFollowsList()
    secondClient.FollowArtists(followedArtists)
    print("done.")

    #TODO
    #Add all playlists
    #clear the account before beggining
    #done.

def LoginUser(firstTime=True,firstClient=None):
    """
    When authenticating for the first time call this function without parameters. Otherwise set the 'firstTime' flag to false and provide the firstClient parameter
    """
    global sourceClient
    client = SpotifyClient()
    if not firstTime:
        while sourceClient.userid == client.userid: #loop to make shure accounts have different userids
            print("Cannot copy from and to the same account.")
            changeChoice = ''
            while changeChoice not in ('1','2'): #loop to make shure user types '1' or '2'
                changeChoice = input("Change [1]st account or [2]nd?: ")

            if changeChoice == '1':
                firstClient = LoginUser(firstTime=False)
                sourceClient = firstClient
            else:
                client = LoginUser(firstTime=False)
        

    print(f"Welcome, {client.name} ({client.email})")
    return client

if __name__ == "__main__":
    main()