import spotipy
from spotipy.oauth2 import SpotifyOAuth
import os
from secrets import client_secret

class SpotifyClient:
    sp = ''
    email = ''
    name = ''
    userid = ''

    def __init__(self, scope="user-follow-modify user-library-modify user-library-read user-read-email user-follow-read"):

        #pull the variables from environment or set them here
        if not os.getenv('CLIENT_ID'):
            client_id = '9df418aac8494a388df6f082a8b94f35'
            os.environ['CLIENT_ID'] = '9df418aac8494a388df6f082a8b94f35'
        else:
            client_id = os.getenv('CLIENT_ID')

        if not os.getenv('REDIRECT_URI'):
            redirect_uri='http://127.0.0.1:9090/callback'
            os.environ['REDIRECT_URI'] = 'http://127.0.0.1:9090/callback'
        else:
           redirect_uri=os.getenv('REDIRECT_URI')

        #Client Secret is pulled from a secrets.py file or SPOTIPY_CLIENT_SECRET environment variable.

        try:
            os.remove(".cache")
            token = spotipy.util.prompt_for_user_token(client_id=client_id, client_secret=client_secret, redirect_uri=redirect_uri,scope=scope,show_dialog=True)
            self.sp = spotipy.Spotify(auth=token)

        except FileNotFoundError as e:
            token = spotipy.util.prompt_for_user_token(client_id=client_id, client_secret=client_secret, redirect_uri=redirect_uri, scope=scope, show_dialog=True)
            self.sp = spotipy.Spotify(auth=token)

        except Exception as e:
            raise(e)
        
        #populate user information variables
        info = self.sp.current_user()
        self.email = info['email']
        self.name = info['display_name']
        self.userid = info['id']


    def GetSavedTracksList(self):
        totalTracks = self.sp.current_user_saved_tracks()['total']
        trackList = []
        offset = 0
        while len(trackList) < totalTracks:
            tracks = self.sp.current_user_saved_tracks(offset=offset)
            for track in tracks['items']:
                trackList.append(track)
            offset +=20
        trackList.reverse()
        return trackList
        
    def GetFollowsList(self):
        totalFollows = self.sp.current_user_followed_artists()['artists']['total']
        response = self.sp.current_user_followed_artists()
        followsList = []
        for item in response['artists']['items']: #first iteration
                followsList.append(item['id'])
        after = followsList[len(followsList)-1]

        while len(followsList) < totalFollows:
            response = self.sp.current_user_followed_artists(after=after)
            for item in response['artists']['items']:
                followsList.append(item['id'])
            after = followsList[len(followsList)-1]
        return followsList

    def GetAlbumsList(self):
        totalAlbums = self.sp.current_user_saved_albums()['total']
        uris = []
        offset = 0
        while len(uris) < totalAlbums:
            albums = self.sp.current_user_saved_albums(offset=offset)
            for album in albums['items']:
                uris.append(album['album']['uri'])

            offset+=20

        return uris

    def GetPlaylists(self):
        pass

    def SaveAlbums(self, uriList):
        #save 20 at a time
        after = 0
        while after < len(uriList):
            try:
                if after+20 >= len(uriList):
                    self.sp.current_user_saved_albums_add(uriList[after:])
                else:
                    self.sp.current_user_saved_albums_add(uriList[after:after+20])
            except Exception as e:
                print(e)
            after += 20

    def FollowArtists(self, uriList):
        #save 20 at a time
        after = 0
        while after < len(uriList):
            try:
                if after+20 >= len(uriList):
                    self.sp.user_follow_artists(uriList[after:])
                else:
                    self.sp.user_follow_artists(uriList[after:after+20])
            except Exception as e:
                print(e)
            after += 20
        

    def SaveTracks(self, tracksList):
        currentSavedTracks = self.GetSavedTracksList() 
        songsToDelete = []
        if currentSavedTracks:
            for track in currentSavedTracks:
                songsToDelete.append(track['track']['uri'])
            
            #Delete Current Tracks in Library 20 at a time
            after = 0
            while after < len(songsToDelete):
                try:
                    if after+20 >= len(songsToDelete):
                        self.sp.current_user_saved_tracks_delete(songsToDelete[after:])
                    else:
                        self.sp.current_user_saved_tracks_delete(songsToDelete[after:after+20])
                except Exception as e:
                    print(e)
                after += 20 

        uris = []
        for track in tracksList:
            uris.append(track['track']['uri'])
        #save [step] at a time
        after = 0
        step = 1
        while after < len(uris):
            try:
                if after+step >= len(uris):
                    self.sp.current_user_saved_tracks_add(uris[after:])
                else: 
                    self.sp.current_user_saved_tracks_add(uris[after:after+step])
            except Exception as e:
                print(e)
            after += step
